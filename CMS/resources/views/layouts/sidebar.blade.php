<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="{{ asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
        <span class="brand-text font-weight-light">ICOM</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ session('username') }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Bảng điều khiển
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.document.create') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Tạo trình ký
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.document.approve_list') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <span class="badge badge-info right">6</span>
                        <p>
                            Phê duyệt
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.document.regain') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <span class="badge badge-info right">6</span>
                        <p>
                            Thu hồi
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.document.process') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <span class="badge badge-info right">6</span>
                        <p>
                            Đang duyệt
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.document.complete') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <span class="badge badge-info right">1.000</span>
                        <p>
                            Hoàn thành
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.document.refuse') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <span class="badge badge-info right">6</span>
                        <p>
                            Từ chối
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.user.list') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Danh sách người dùng
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.department.list') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Danh sách phòng ban
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.document.document_list') }}" class="nav-link">
                        <i class="nav-icon fas fa-edit"></i>
                        <p>
                            Danh sách trình ký
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>