<?php

namespace App\Providers;

use App\Helpers\ExternalApiHelper;
use App\Helpers\Logger;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind(ExternalApiHelper::class, function() {
        //     return new ExternalApiHelper('Hello, app!');
        // });

        // $this->app->singleton(Logger::class, function() {
        //     return new Logger();
        // });
    }
}
