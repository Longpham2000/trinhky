<?php
// use Illuminate\Routing\Route;


$namespace = 'App\Modules\Dashboard\Controllers';

Route::group([
    'middleware' => 'web',
    'module' => 'Dashboard',
    'namespace' => $namespace
    ], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/dashboard', 'DashboardController@dashboard')->name('admin.dashboard');
        
    });

    
});