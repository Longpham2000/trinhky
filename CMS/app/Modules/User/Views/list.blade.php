@extends('layouts.master')

@section('title','Danh sách người dùng')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">

        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="modal-title">Danh sách người dùng</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
    @if(session('thongbao'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('thongbao')}} ...
            </div>
            @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    <div class="float-left">                            
                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add_user">
                                Thêm người dùng
                            </a>  
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover projects">
                            <thead>
                                <tr>
                                <th class="serial">#</th>
                                    <th>Avatar</th>
                                    <th>Tên đăng nhập</th>
                                    <th>Tên hiển thị</th>
                                    <th>Chức vụ</th>
                                    <th>Phòng ban</th>
                                    <th>Người tạo</th>
                                    <th>Ngày tạo</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $id=1;                                    
                                ?> 
                                @foreach ($users as $data)
                                <tr>
                                    <td  class="serial"> {{$id}}</td>
                                    <td>
                                        <img alt="Avatar" class="table-avatar" src="{{ asset('admin/dist/img/avatar.png')}}">
                                    </td>
                                    <td>{{ $data->username }}</td>
                                    <td>{{ $data->nickname }}</td>
                                    <td>{{ $data->role_name }}</td>
                                    <td>{{ $data->department_name }}</td>
                                    <td>{{ $data->user_create }}</td>
                                    <td>{{ $data->time_create }}</td>
                                    <td>
                                        <button type="button" class="btn btn-warning" onclick="thaydoimatkhau({{$data->id}})">Lấy lại mật khẩu</button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_user">Xóa người dùng</button>
                                        <button type="button" class="btn btn-success" onclick="thaydoiuser({{$data->id}})">Cập nhật</button>
                                        <button type="button" class="btn btn-info" onclick="phanquyen({{$data->id}})">Phân quyền</button>
                                    </td>                                   
                                </tr>
                                <?php $id++; ?>  
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                
            </div>
        </div>
    </div>
</div>
<script>
    function thaydoimatkhau(id) {
        $('#recovery_password').modal('show');
        $("#id").val(id);
    }
    function thaydoiuser(id) {
        $('#update_user').modal('show');
        $("#id").val(id);
    }
    function phanquyen(id) {
        $('#role_user').modal('show');
        $("#id").val(id);
    }
</script>
<div class="modal fade" id="add_user">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Thêm Người Dùng</h4>
            </div>
            <form action="{{url('admin/create_user')}}" method="post">
                 {{csrf_field()}} 
                 <div class="modal-body">
                 <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                        <input type=hidden name="id" id="id" />
                            <input type="tex" id="name-input" name="email" placeholder="Tên đăng nhập" class="form-control">
                        </div> 
                    </div>                    
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="is_admin" placeholder="Nhập tên hiển thi" class="form-control">
                        </div> 
                    </div> 
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="mat_khau" placeholder="Nhập mật khẩu" class="form-control">
                        </div> 
                    </div>
                    <div class="row form-group">
                        <div class="col-12 col-md-9">
                        <select class="form-control" name="phong_ban">
                            <option>-- Chọn phòng ban --</option>
                            @foreach($phongban as $pb)
                            <option value="{{ $pb->id}}">{{$pb->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="nguoi_tao" placeholder="Người tạo" class="form-control">
                        </div> 
                    </div>
                    </div> 
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
<div class="modal fade" id="update_user">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Sửa Người Dùng</h4>
            </div>
            <form action="{{url('admin/update')}}" method="post" >
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                        <input type=hidden name="id" id="id" />
                            <input type="tex" id="name-input" name="email" placeholder="Tên đăng nhập" class="form-control">
                        </div> 
                    </div>                    
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="is_admin" placeholder="Nhập tên hiển thi" class="form-control">
                        </div> 
                    </div> 
                    <div class="row form-group">
                        <div class="col-12 col-md-9">
                        <select class="form-control" name="phong_ban">
                            <option>-- Chọn phòng ban --</option>
                            @foreach($phongban as $pb)
                            <option value="{{ $pb->id}}">{{$pb->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="nguoi_cap_nhat" placeholder="Người cập nhật" class="form-control">
                        </div> 
                    </div>
                    </div> 
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
<div class="modal fade" id="recovery_password">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Lấy lại mật khẩu</h4>
            </div>
        <div class="modal-body">
            <form action="{{url('admin/update_pass')}}" method="post">
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class="form-group">
                        <input type=hidden name="id" id="id" />
                        <input type="password" class="form-control" id="exampleInputPassword1" name="password"
                            placeholder="Nhập mật khẩu mới">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="exampleInputPassword1"
                            placeholder="Nhập lại mật khẩu mới">
                    </div>
                </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
<div class="modal fade" id="role_user">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Phân quyền</h4>
            </div>
            <div class="modal-body">
            <form action="{{url('admin/update_department')}}" method="post">
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class=" row form-group">
                        <div class="col-12 col-md-9">
                        <select class="form-control" name="phong_ban">
                            <option>-- Chọn phòng ban --</option>
                            @foreach($phongban as $pb)
                            <option value="{{ $pb->id}}">{{$pb->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                             <input type=hidden name="id" id="id" />
                            <input type="tex" id="name-input" name="nguoi_cap_nhat" placeholder="Người cập nhật" class="form-control">
                        </div> 
                    </div>
                    </div> 
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
<div class="modal fade" id="delete_user">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Xóa Người Dùng</h4>
            </div>
            <form action="{{url('admin/delete')}}" method="post">
                 {{csrf_field()}} 
                    <div class="modal-body">
                        <p>Bạn có muốn xóa người dùng này?</p>
                    </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Đồng ý</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>


@endsection