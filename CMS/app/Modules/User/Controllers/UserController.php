<?php

namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\connect;
use App\checksessoin;

class UserController extends Controller
{
    public function index(){
          return view('User::index');
    }
    public function login(Request $request){
        try{
            $api=new connect();
            $postdata = (
                array(
                    'password' => $request->password,
                    'username'=>$request->username
                )
            );     
            $repson=json_decode( $api->apipost('http://14.160.70.230:11080/IcomESignature/api/login', $postdata));
            if( $repson->status) {
                $request->session()->put('token', $repson->data->token); 
                $request->session()->put('username', $repson->data->profile->username);
                $request->session()->put('nickname', $repson->data->profile->nickname);
                $request->session()->put('department_name', $repson->data->profile->department_name);         
                return view('layouts.master');
            }
            else
    
            return  view('User::index');
        }catch (\Exception $e){
            die($e->getMessage());
        }
        
    }
    public function logout(Request $request){
        return redirect()->route('index');
        
    }
    public function danhsach(){
        $api = new connect();
        $data = json_decode($api->apiget('http://14.160.70.230:11080/IcomESignature/api/admin_user/list?page_index=1&page_size=10'));
        $users = $data->data;
        $phongban= json_decode($api->apiget('http://14.160.70.230:11080/IcomESignature/api/public_data/list_department'));
        $phongban=$phongban->data;
        return view('User::list', compact('users','phongban'));
    }
    public function update_pass(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                'id'=>$request->id,
                'mat_khau' => $request->password,
                "nguoi_cap_nhat"=>session('username')
                
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/admin_user/update_pass', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/user_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/user_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }
    public function update_department(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                "id"=> $request->id,
                "nguoi_cap_nhat"=> $request->nguoi_cap_nhat,
                "phong_ban"=> $request->phong_ban               
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/admin_user/update_department', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/user_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/user_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }
    public function update_user(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                "id"=> $request->id,
                "email"=> $request->email,
                "is_admin"=> $request->is_admin,
                "nguoi_cap_nhat"=> $request->nguoi_cap_nhat,
                "phong_ban"=> $request->phong_ban,
                "status"=> 1,
                "ten_hien_thi"=> $request->ten_hien_thi,
                "token"=> $request->token
                
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/admin_user/update', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/user_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/user_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }
    public function checkemail($email){
        $api = new connect();
      $postdata = (
         array(
             'email'=>$email,   
         )
     );
     $repson = json_decode($api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/admin_user/checkemail',$postdata ));
      if($repson->status==1) return 1;
      else return 0;
    }
    public function create_email( Request $request){
        try{
            $api = new connect();
            if ($this->checkemail($request->email))
            return redirect('');
            $postdata = 
             array(
                "email"=> $request->email,
                "is_admin"=> $request->is_admin,
                "mat_khau"=> $request->mat_khau,
                "nguoi_tao"=> $request->nguoi_tao,
                "phong_ban"=> $request->phong_ban,
                "status"=> 1,
                "ten_hien_thi"=> $request->ten_hien_thi  
            );  
         ( $repson = json_decode($api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/admin_user/create',json_encode($postdata ))));
         if($repson == null){
            die("api return null");
        }
            if($repson->status==1) 
         return redirect('admin/user_list')->with('thongbao',$repson->msg);
         return redirect()->to('admin/user_list');
         }catch (\Exception $e){ 
             die($e->getMessage());
         }
        } 
    
    

   
  
    
}
