<?php
$namespace = 'App\Modules\User\Controllers';

Route::group([
    'middleware' => 'web',
    'module' => 'User',
    'namespace' => $namespace
    ], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/user_list', 'UserController@danhsach')->name('admin.user.list');
        Route::post('update', 'UserController@update_user');
        Route::post('update_pass', 'UserController@update_pass');
        Route::post('update_department', 'UserController@update_department');
        Route::post('create_user', 'UserController@create_email');

    });
    Route::group(['prefix' => '/'], function () {
        Route::get('/', 'UserController@index');
        Route::post('login', 'UserController@login');
        Route::post('logout', 'UserController@logout');
       
    });
});