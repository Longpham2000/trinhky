
@extends('layouts.master')
@section('title','Danh sách phòng ban')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="modal-title">Danh sách phòng ban</h1>
            </div><!-- /.col -->
        </div><!-- /.container-fluid -->
    </div>
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
    @if(session('thongbao'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('thongbao')}} ...
            </div>
            @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    <div class="float-left">                            
                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add_room">
                                Thêm phòng ban
                            </a>                                
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover projects">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mã phòng ban</th>
                                    <th>Tên phòng ban</th>
                                    <th>Người tạo</th>
                                    <th>Ngày tạo</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $id=1?>
                                @foreach ($repson as $room)
                                    <tr>
                                    <td  class="serial"> {{$id}}</td>
                                        <td>{{ $room->ma }}</td>
                                        <td>{{ $room->ten }}</td>
                                        <td>{{ $room->nguoi_tao }}</td>
                                        <td>{{ $room->ngay_tao }}</td>
                                        <td>
                                            <button type="button" class="btn btn-warning" onclick="capnhatban({{$room->id}})">Cập nhật</button>
                                           <button type="button" class="btn btn-danger" onclick="xoaban({{$room->id}})" >Xóa phòng ban</button>
                                        </td>                                       
                                    </tr>
                                    <?php $id++; ?> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_room">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Thêm Phòng Ban</h4>
            </div>
            <form action="{{url('admin/create_department')}}" method="post">
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ma" placeholder="Nhập mã phòng ban" class="form-control">
                        </div> 
                    </div> 
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ten" placeholder="Nhập tên phòng ban" class="form-control">
                        </div>                             
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="nguoi_tao" placeholder="Người tạo" class="form-control">
                        </div> 
                    </div>  
                    </div> 
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
<script>
    function capnhatban(id) {
        $('#update_ban').modal('show');
        $("#id").val(id);
    }
    function xoaban(id) {
        $('#delete_ban').modal('show');
        $("#id").val(id);
    }
</script>
<div class="modal fade" id="update_ban">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Sửa Phòng Ban</h4>
            </div>
            <form action="{{url('admin/update_department')}}" method="post">
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class="row form-group"> 

                        <div class="col-12 col-md-9">
                        <input type=hidden name="id" id="id" />
                            <input type="tex" id="name-input" name="ma" placeholder="Nhập mã phòng ban" class="form-control">
                        </div> 
                    </div> 
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ten" placeholder="Nhập tên phòng ban" class="form-control">
                        </div>                             
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="nguoi_cap_nhat" placeholder="Người cập nhật" class="form-control">
                        </div> 
                    </div>
                    </div> 
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary" >Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>   
    </div>
</div>
<div class="modal fade" id="delete_ban">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Xóa Phòng Ban</h4>
            </div>
            <form action="{{url('admin/delete_department')}}" method="post">
                 {{csrf_field()}} 
                 @method('delete')
                    <div class="modal-body">
                        <input type=hidden name="id" id="id" />
                        <p>Bạn có muốn xóa phòng ban này?</p>
                    </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Đồng ý</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
@endsection
