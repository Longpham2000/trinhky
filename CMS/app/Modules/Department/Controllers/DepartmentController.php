<?php

namespace App\Modules\Department\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\connect;


class DepartmentController extends Controller
{
    public function danhsach(){
        $api=new connect();
        $repson=json_decode( $api->apiget('http://14.160.70.230:11080/IcomESignature/api/department/list?page_index=1&page_size=10'));
        $repson=$repson->data;
        $phongban= json_decode($api->apiget('http://14.160.70.230:11080/IcomESignature/api/public_data/list_department'));
        $phongban=$phongban->data;
    	return view('Department::list',compact('repson','phongban'));
    }
    public function create_department(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                "id_cha"=> $request->id_cha,
                "ma"=> $request->ma,
                "nguoi_tao"=> $request->nguoi_tao,
                "status"=> 1,
                "ten"=> $request->ten
                
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/department/create', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/department_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/department_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }
    public function update_department(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                "id"=> $request->id,
                "id_cha"=> $request->id_cha,
                "ma"=> $request->ma,
                "nguoi_cap_nhat"=> $request->nguoi_cap_nhat,
                "status"=> 1,
                "ten"=> $request->ten
                
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/department/update', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/department_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/department_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }
    public function delete_department($id){
        
    }
}
