<?php
$namespace = 'App\Modules\Department\Controllers';

Route::group([
    'middleware' => 'web',
    'module' => 'Department',
    'namespace' => $namespace
    ], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/department_list', 'DepartmentController@danhsach')->name('admin.department.list');
        Route::post('create_department','DepartmentController@create_department');
        Route::post('update_department','DepartmentController@update_department');
        Route::post('delete_department','DepartmentController@delete_department');
    });
});