<?php
$namespace = 'App\Modules\Document\Controllers';

Route::group([
    'middleware' => 'web',
    'module' => 'Document',
    'namespace' => $namespace
    ], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/document_list','DocumentController@danhsach')->name('admin.document.document_list');
        Route::post('add_document','DocumentController@add_document');
        Route::post('update_document','DocumentController@update_document');

        Route::get('approve_list','DocumentController@phe_duyet')->name('admin.document.approve_list');
        Route::post('approve_action', 'DocumentController@xuly_pheduyet')->name('admin.document.approve_action');
        Route::post('cancel_action', 'DocumentController@xuly_thuhoi')->name('admin.document.cancel_action');
        Route::post('update_action', 'DocumentController@xuly_update')->name('admin.document.update_action');
        Route::get('download_mau/{id}', 'DocumentController@download_mau')->name('admin.document.download_mau');
        
        Route::get('create','DocumentController@tao_trinh_ky')->name('admin.document.create');
        Route::get('refuse','DocumentController@tu_choi')->name('admin.document.refuse');
        Route::get('process','DocumentController@dang_duyet')->name('admin.document.process');
        Route::get('complete','DocumentController@hoan_thanh')->name('admin.document.complete');  
        Route::get('cancel','DocumentController@thu_hoi')->name('admin.document.regain');
    });
});