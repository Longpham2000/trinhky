
@extends('layouts.master')

@section('title','Tạo trình ký')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                
                <div class="col-md-12">

                    <div class="card card-primary">
                        <div class="card-header">
                            <h2 class="card-title">Bước 1. Tải mẫu trình ký (Có thể bỏ qua bước này nếu đã có file mẫu)</h2>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                            <div class="card-body">                                
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Trình ký</label>
                                    <div class="col-sm-10">
                                        <select class="form-control">
                                            <option>--- Chọn trình ký ---</option>
                                            <option>Đề xuất nhân sự</option>
                                            <option>Bàn giao tài sản</option>
                                            <option>Tạm ứng dự án</option>
                                            <option>Khác</option>
                                        </select>
                                        <small class="form-text text-muted">Sau khi chọn trình ký, nhấn "Tải trình ký" để lấy mẫu. Nếu không có mẫu phù hợp, tải mẫu "Khác"</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Tải xuống</label>
                                    <div class="col-sm-10">
                                        <button class="btn btn-sm btn-info">Tải trình ký</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <div class="card card-primary">
                        <div class="card-header">
                            <h2 class="card-title">Bước 2. Yêu cầu phê duyệt</h2>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form  action="#">
                            <div class="card-body">                                
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">File trình ký</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="exampleInputFile">
                                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                            </div>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="">Upload</span>
                                            </div>
                                        </div>
                                        <small class="form-text text-muted">Chọn file trình ký đã điền thông tin được tải xuống ở bước "Bước 1. Tải mẫu trình ký"</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Trình ký</label>
                                    <div class="col-sm-10">
                                        <select class="form-control">
                                            <option>--- Chọn trình ký ---</option>
                                            <option>Đề xuất nhân sự</option>
                                            <option>Bàn giao tài sản</option>
                                            <option>Tạm ứng dự án</option>
                                            <option>Khác</option>
                                        </select>
                                        <small class="form-text text-muted">Chọn trình ký tương ứng với file upload</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="user1" class="">Người duyệt cấp 1</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="user1" placeholder="Nhập email người duyệt cấp 1, ngăn cách bởi dấu  phẩy ',' Nhập tối đã 3 người">
                                        <small class="form-text text-muted">Nguyễn Văn A (nguyenvana@gmail.com)</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="user2" class="">Người duyệt cấp 2</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="user2" placeholder="Nhập email người duyệt cấp 2, ngăn cách bởi dấu  phẩy ',' Nhập tối đã 3 người">
                                        <small class="form-text text-muted">Nguyễn Văn B (nguyenvanb@gmail.com)</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="user3" class="">Người duyệt cấp 3</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="user3" placeholder="Nhập email người duyệt cấp 3, ngăn cách bởi dấu  phẩy ',' Nhập tối đã 3 người">
                                        <small class="form-text text-muted">Nguyễn Văn C (nguyenvanc@gmail.com)</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="user4" class="">Người duyệt cấp 4</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="user4" placeholder="Nhập email người duyệt cấp 4, ngăn cách bởi dấu  phẩy ',' Nhập tối đã 3 người">
                                        <small class="form-text text-muted">Nguyễn Văn D (nguyenvand@gmail.com)</small>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="inputEmail3" class="">Quan trọng</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                            <label class="form-check-label" for="exampleCheck1">Yêu cầu phê duyệt gấp</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Ghi chú</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" placeholder="Nhập nội dung ghi chú..."></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="inputEmail3" class="">Trình ký</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <button class="btn btn-sm btn-info">Thực hiện</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                
                            
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

@endsection





