
@extends('layouts.master')

@section('title','Danh sách phê duyệt')
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        @if(session('thongbao'))
            <div class="alert alert-primary">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('thongbao')}} ...
            </div>
            @endif
        <div class="row mb-2">
            <div class="col-6">
                {{-- <h1 class="m-0 text-dark">Danh sách Luồng duyệt</h1> --}}
                
            </div><!-- /.col -->
            <div class="col-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="index">Home</a></li>
                    <li class="breadcrumb-item active">Starter Page</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                            <strong class="card-title">Danh sách phê duyệt</strong>
                    </div>
                    <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover projects">
                    <thead>
                    <tr>
                                            <th class="serial">#</th>
                                            <th>Tên trình ký</th>
                                            <th>Người tạo</th>
                                            <th>Người duyệt</th>
                                            <th>Thời gian tạo</th>
                                            <th>Mức độ</th>
                                            <th>Hành động</th>
                                            <th>Xử lý</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $stt=1; ?>     
                                    @foreach($repson as $data)
                                        <tr>
                                            <td  class="serial"> {{$stt}}</td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->user_create}}</td>
                                            <td>{{$data->approve_list}}</td>
                                            <td>{{$data->time_create}}</td>
                                            <td class="text-center">
                                                @isset($data->important)
                                                @if($data->important == 1)
                                                <span class="right badge badge-danger">Ưu tiên</span>
                                                @else
                                                <span class="right badge badge-success">Bình thường</span>
                                                @endif
                                                @endisset

                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{$data->link}}">Tải xuống</a>
                                            </td>
                                            <td>
                                                <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#approved{{$stt}}">Duyệt</span>
                                                <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#denied{{$stt}}">Từ chối</span>
                                            </td>               
                                        </tr>
                                        <div class="modal fade" id="approved{{$stt}}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Phê duyệt</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h3>Duyệt bản Trình ký này</h3>
                                                    </div>

                                                    <div class="modal-footer ">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                        <form action="{{route('admin.document.approve_action')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$data->id}}">
                                                            <input type="hidden" name="approve" value="1">
                                                            <input type="submit" class="btn btn-primary" value="Thực hiện">
                                                        </form>
                                                        

                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                        <div class="modal fade" id="denied{{$stt}}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Từ chối duyệt</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>           
                                                    <form action="{{route('admin.document.approve_action')}}" method="post" class="form-horizontal">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$data->id}}">
                                                        <input type="hidden" name="approve" value="0">
                                                        <div class="modal-body">
                                                            <div class="row form-group">
                                                                <div class="col-12 col-md-9"><textarea required name="note" id="textarea-input" rows="3" placeholder="Lý do từ chối" class="form-control"></textarea></div>
                                                        </div>
                                                                
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                            <button type="submit" class="btn btn-primary">Thực hiện</button>
                                                    </div>
                                                </form>
                                                </div>
                                            </div>
                                        </div>
                                    <?php $stt++ ?>                        
                                    @endforeach
                                    </tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.card -->
                {{-- <div class="card-header">
                                <div class="row form-group">
                                    <div class="col-12 col-md-5"><strong class="card-title">Đang xem 6 trong tổng số 66</strong></div>
                                    <div class="col-12 col-md-7">
                                        <span class="btn btn-info btn-sm">1</span>
                                        <span class="btn btn-info btn-sm">4</span>
                                        <span class="btn btn-info btn-sm">5</span>
                                        <span class="btn btn-info btn-sm">6</span>
                                        <span class="btn btn-primary btn-sm">7</span>
                                    </div>
                                </div>
                </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection