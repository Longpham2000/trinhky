
@extends('layouts.master')
@section('title','Danh sách đang duyệt')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">

    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
         @if(session('thongbao'))
            <div class="alert alert-primary">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('thongbao')}} ...
            </div>
            @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="modal-title">Tìm Kiếm </h5>
                    </div>
                    <div class="card-body">
                    <div class="modal-body">
                        <form action="#" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col-12 col-md-5">
                                    <select name="selectSm" id="selectSm" class="form-control-sm form-control">
                                        <option value="0">--- Tất cả trình ký ---</option>
                                        <option value="1">Bàn giao công việc</option>
                                        <option value="2">Đề xuất nhân sự</option>
                                    </select>
                                </div>
                    <div class="col-12 col-md-2"><span class="btn btn-primary btn-sm">Tìm kiếm</span></div>                                        
                            </div>
                        </form>
                </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="modal-title">Danh sách đang duyệt</h5>
                    </div>

                    <table id="example2" class="table table-bordered table-hover projects">
                                    <thead>
                                    <tr>
                                            <th class="serial">#</th>
                                            <th>Mã</th>
                                            <th>Trình ký</th>
                                            <th>Người xử lý</th>
                                            <th>Ngày xử lý</th>
                                            <th>Đang duyệt</th>
                                            <th>Thu hồi</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1?>               
                                        @foreach($repson as $data)
                                    <tr>
                                        <td class="serial">{{$i}}</td>
                                        <td>{{$data->id}}</td>                                       
                                        <td>{{$data->tenTrinhKy}}</td>
                                        <td>{{$data->nguoiTao}}</td>                                      
                                        <td>{{ \Carbon\Carbon::parse($data->ngayCapNhat)->format('d/m/Y') }}</td>
                                        <td>{{$data->nguoiCapNhat}}</td>
                                        <td>
                                            <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#cancel{{$i}}" >Thu hồi</span>
                                            <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#update{{$i}}" >Sửa</span>
                                        </td>
                                        <td>
                                        <a href="{{$data->fileTrinhKy}}" class="btn btn-info btn-sm">Tải xuống</a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="update{{$i}}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Sửa Trình ký</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>           
                                                    <form action="{{route('admin.document.update_action')}}" method="post" class="form-horizontal">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$data->id}}">
                                                        <div class="modal-body">
                                                            <div class="row form-group">
                                                                <div class="col-12 col-md-9">
                                                                    <label for="link">Nhập đường dẫn: </label>
                                                                    <input class="form-control" type="text" name="link" id="link" required placeholder="VD: https://link.com">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-12 col-md-9">
                                                                    <label for="link">Nhập lý do: </label>
                                                                    <textarea required name="note" id="textarea-input" rows="3" placeholder="Lý do thu hồi" class="form-control" ></textarea>
                                                                </div>
                                                            </div>
                                                                
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                                <button type="submit" class="btn btn-primary">Thực hiện</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    <div class="modal fade" id="cancel{{$i}}">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Thu hồi</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>           
                                                    <form action="{{route('admin.document.cancel_action')}}" method="post" class="form-horizontal">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$data->id}}">
                                                        <div class="modal-body">
                                                            <div class="row form-group">
                                                                <div class="col-12 col-md-9"><textarea required name="note" id="textarea-input" rows="3" placeholder="Lý do thu hồi" class="form-control"></textarea></div>
                                                            </div>
                                                                
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                                                                <button type="submit" class="btn btn-primary">Thực hiện</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                    </div>

                                    
                                </tbody>
                                    <?php $i++ ?>
                                    @endforeach    
                                </table>
                            </div> <!-- /.table-stats -->
                <!-- /.card -->
                <div class="card-header">
                                <div class="row form-group">
                                    <div class="col-12 col-md-8"><strong class="card-title">Đang xem 6 trong tổng số 66</strong></div>
                                    <div class="col-12 col-md-4">
                                        <span class="btn btn-info btn-sm">1</span>
                                        <span class="btn btn-info btn-sm">4</span>
                                        <span class="btn btn-info btn-sm">5</span>
                                        <span class="btn btn-info btn-sm">6</span>
                                        <span class="btn btn-primary btn-sm">7</span>
                                    </div>
                                </div>
                            </div>
            </div>
        </div>
    </div>
</div>





@endsection