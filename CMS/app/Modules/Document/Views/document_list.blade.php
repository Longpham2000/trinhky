
@extends('layouts.master')
@section('title','Danh sách trình ký')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="modal-title">Danh sách trình ký</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
    @if(session('thongbao'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('thongbao')}} ...
            </div>
            @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    <div class="float-left">                            
                            <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add_tk">
                                Thêm trình ký
                            </a>                                
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover projects">
                        <thead>
                                        <tr>
                                            <th class="serial">#</th>
                                            <th>Mã</th>
                                            <th>Tên</th>
                                            <th>Người tạo</th>
                                            <th>Ngày tạo</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1?>               
                                        @foreach($postdata as $data)
                                    <tr>
                                        <td class="serial">{{$i}}</td>
                                        <td>{{$data->ma}}</td>
                                        <td>{{$data->ten}}</td>
                                        <td>{{$data->nguoi_tao}}</td>
                                        <td>{{$data->ngay_tao}}</td>
                                        <td>
                                        <button type="button" class="btn btn-warning" onclick="capnhattk({{$data->id}})">Cập nhật</button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_tk" >Xóa trình ký</button>
                                        </td>
                                    </tr>
                                    <?php $i++ ?>
                                    @endforeach
                                    </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_tk">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Thêm danh sách trình ký</h4>
            </div>
            <form action="{{url('admin/add_document')}}" method="post" >
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-12 col-md-9"><input type="file" id="name-input" name="file_mau" placeholder="Nhập tên trình ký" class="form-control">
                            <small class="form-text text-muted">File trình ký mẫu</small>
                        </div>
                    </div>                  
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ma" placeholder="Nhập mã trình ký" class="form-control">
                        </div> 
                    </div> 
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ten" placeholder="Nhập tên trình ký" class="form-control">
                        </div>                             
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="nguoi_tao" placeholder="Người tạo" class="form-control">
                        </div> 
                    </div>
                    </div>
                    <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div> 
                    </div>
                </form>
        </div>
    </div>
</div>
<script>
    function capnhattk(id) {
        $('#update_tk').modal('show');
        $("#id").val(id);
    }
</script>
<div class="modal fade" id="update_tk">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Sửa Trình Ký</h4>
            </div>
            <form action="{{url('admin/update_document')}}" method="post" >
                 {{csrf_field()}} 
                 <div class="modal-body">
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                        <input type=hidden name="id" id="id" />
                            <input type="file" id="name-input" name="file_mau" placeholder="Nhập file trình ký" class="form-control">
                        </div> 
                    </div>                    
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ma" placeholder="Nhập mã trình ký" class="form-control">
                        </div> 
                    </div> 
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="ten" placeholder="Nhập tên trình ký" class="form-control">
                        </div>                             
                    </div>
                    <div class="row form-group"> 
                        <div class="col-12 col-md-9">
                            <input type="tex" id="name-input" name="nguoi_cap_nhat" placeholder="Người cập nhật" class="form-control">
                        </div> 
                    </div>
                    </div> 
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Lưu lại</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>
<div class="modal fade" id="delete_tk">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
            <div class="card-header">
                <h4 class="modal-title">Xóa Trình Ký</h4>
            </div>
            <form action="{{ url('cong_cu__danh_sach_trinh_ky')}}" method="post">
                 {{csrf_field()}} 
                    <div class="modal-body">
                        <p>Bạn có muốn xóa trình ký này?</p>
                    </div>
                        <div class="modal-footer"> 
                            <button type="submit" class="btn btn-primary">Đồng ý</button> 
                            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button> 
                        </div>  
            </div>
                </form>
        </div>
    </div>
</div>

@endsection