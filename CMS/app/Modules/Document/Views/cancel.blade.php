
@extends('layouts.master')
@section('title','Danh sách thu hồi')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="modal-title">Tìm kiếm</h5>
                    </div>
                    <div class="card-body">
                    <div class="modal-body">
                    <form action="#" method="post" accept-charset="utf-8" class="form-horizontal">
                                {{ csrf_field() }}    
                                <div class="row form-group">
                                        <div class="col-12 col-md-5">
                                            <select name="selectSm" id="selectSm" class="form-control-sm form-control">
                                            <option value="0">--- Tất cả trình ký ---</option>
                                            <option value="1">Bàn giao công việc</option>
                                            <option value="2">Đề xuất nhân sự</option>
                                            </select>
                                        </div>
                                        <input type="submit" name="" id="sub" value="Tìm Kiếm" class="btn btn-sm btn-primary " class="form-control">
                                        
                                    </div>
                                </form>
                </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="modal-title">Danh sách thu hồi</h5>
                    </div>

                    <table id="example2" class="table table-bordered table-hover projects">
                    <thead>
                    <tr>
                                    <th class="serial">#</th>
                                    <th>Mã</th>
                                    <th>Trình ký</th>
                                    <th>Người xử lý</th>
                                    <th>Ngày xử lý</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1?>               
                                        @foreach($repson as $data)
                                    <tr>
                                        <td class="serial">{{$i}}</td>
                                        <td>{{$data->code}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->user_create}}</td>
                                        <td>{{$data->time_create}}</td>
                                        <td>
                                        <a href="{{$data->link}}" class="btn btn-info btn-sm">Tải xuống</a>
                                        </td>
                                    </tr>
                                    <?php $i++ ?>                        
                                    @endforeach
                                    </tbody>
                        </tbody>
                    </table>
                </div>
                <!-- /.card -->
                <div class="card-header">
                                <div class="row form-group">
                                    <div class="col-12 col-md-8"><strong class="card-title">Đang xem 6 trong tổng số 66</strong></div>
                                    <div class="col-12 col-md-4">
                                        <span class="btn btn-info btn-sm">1</span>
                                        <span class="btn btn-info btn-sm">4</span>
                                        <span class="btn btn-info btn-sm">5</span>
                                        <span class="btn btn-info btn-sm">6</span>
                                        <span class="btn btn-primary btn-sm">7</span>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection