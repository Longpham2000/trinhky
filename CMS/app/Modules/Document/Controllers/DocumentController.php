<?php

namespace App\Modules\Document\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\connect;


class DocumentController extends Controller
{   

    public function danhsach(){
        $api=new connect();
        $repson=json_decode( $api->apiget('http://14.160.70.230:11080/IcomESignature/api/trinh_ky/list?page_index=1&page_size=4'));
        $postdata=$repson->data;
    	return view('Document::document_list',compact('postdata'));
    }
    public function add_document(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                "file_mau"=> $request->file_mau,
                "ma"=> $request->ma,
                "nguoi_tao"=> $request->nguoi_tao,
                "status"=>1,
                "ten"=> $request->ten
                
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/trinh_ky/create', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/document_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/document_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }
    public function update_document(Request $request){
        try{
            $api=new connect();
            $postdata = 
           
            array(
                "id"=>$request->id,
                "file_mau"=> $request->file_mau,
                "ma"=> $request->ma,
                "nguoi_cap_nhat"=> $request->nguoi_cap_nhat,
                "status"=> 1,
                "ten"=> $request->ten
                
            );
           (    $repson=json_decode( $api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/trinh_ky/update', json_encode($postdata))));
           if($repson == null){
               die("api return null");
           }
           if($repson->status==1) 
            return redirect('admin/document_list')->with('thongbao',$repson->msg);
            return redirect()->to('admin/document_list');
            }catch (\Exception $e){ 
                die($e->getMessage());
            } 
    }


    public function download_mau($id){
        $api = new connect();
        $repson=json_decode($api->apiget("http://14.160.70.230:11080/IcomESignature/api/document/download?id=$id"));
        $repson=$repson->data;
        
        return view('Document::approve_list',compact('repson'));
    }
    public function phe_duyet(){
        $api=new connect();

        $repson=json_decode(    $api->apiget("http://14.160.70.230:11080/IcomESignature/api/document/approve_list?page_index=1&page_size=10"));
        $repson=$repson->data;
    	return view('Document::approve_list',compact('repson'));
    }
    public function xuly_pheduyet(Request $request){
            $request->validate([
                'approve'=>"required",
                "id"=>"required"
            ]);
            
            $api=new connect();

            if ($request->has('note')) {
                # code...
                $note = $request->note;
            }else $note = null;

            $postdata = 
                array(
                    "approve"=> $request->approve,
                    "id"=> $request->id,
                    "note"=> htmlspecialchars($note),
                );
                
            $repson = json_decode($api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/document/approve_action', json_encode($postdata)));
            return back()->with('thongbao',$repson->msg);
        }
    public function xuly_thuhoi(Request $request){
        $api=new connect();

        $request->validate([
            "id" => "required",
            "note"=> "required"
        ]);

        $postdata = 
                array(
                    "id"=> $request->id,
                    "note"=> htmlspecialchars($request->note),
                );

         $repson = json_decode($api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/document/cancel_action', json_encode($postdata)));
        return back()->with('thongbao',"Thu hồi ".$repson->msg);
        

    }
    public function xuly_update(Request $request){
        $api=new connect();
        
        $request->validate([
            "id" => "required",
            "link" => "required",
            "note"=> "required"
        ]);
        dd();
        $postdata = 
                array(
                    "document_link"->$request->link,
                    "id"=> $request->id,
                    "note"=> htmlspecialchars($request->note),
                );

         $repson = json_decode($api->apipostadd('http://14.160.70.230:11080/IcomESignature/api/document/cancel_action', json_encode($postdata)));
        return back()->with('thongbao',"Chỉnh sửa ".$repson->msg);
        

    }
    public function tao_trinh_ky(){
    	return view('Document::create');
    }
    public function tu_choi(){   
        $api=new connect();
        $repson=json_decode(    $api->apiget('http://14.160.70.230:11080/IcomESignature/api/document/refuse?page_index=1&page_size=4'));
        $repson=$repson->data; 
              
        return view('Document::refuse',compact('repson'));
    }
    public function dang_duyet(){
        $api=new connect();
        $repson=json_decode(    $api->apiget('http://14.160.70.230:11080/IcomESignature/api/document/process?page_index=1&page_size=10'));
        $repson=$repson->data; 
        return view('Document::process',compact('repson'));
    }
    public function hoan_thanh(){
        $api=new connect();
        $repson=json_decode(    $api->apiget('http://14.160.70.230:11080/IcomESignature/api/document/complete?page_index=1&page_size=10'));
        $postdata=$repson->data; 
        return view('Document::complete',compact('postdata'));
    }
    public function thu_hoi(){
        $api=new connect();
        $repson=json_decode(    $api->apiget('http://14.160.70.230:11080/IcomESignature/api/document/cancel_list?page_index=1&page_size=4'));
        $repson=$repson->data; 
        return view('Document::cancel',compact('repson'));
    }
}
