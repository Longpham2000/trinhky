<?php 
namespace App;
class connect{
    public  function login($url,$data){
        $curl = curl_init();
        $token=session('token');;
        curl_setopt_array($curl, array(      
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",       
        CURLOPT_POSTFIELDS =>json_encode($data),      
        CURLOPT_HTTPHEADER => array(
            "accept: application/json",   
            "content-type: application/json",          
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            return "";
        }else{
            return $response;
        }
    }
    public function apipost($url,$data){
       if($json = @file_get_contents("http://14.160.70.230:11080/IcomESignature/swagger-ui.html#", true)){
        $opts = array('http' =>
                        array(
                            'method'  => 'POST',
                            'header'  => 'Content-Type: application/json',
                            'content' =>json_encode( $data)
                        )
                    );
      
        $context  = stream_context_create($opts);
        $result = file_get_contents($url, false, $context);
        return  $result ;
                }
        else{
            return "";
        }
    }
    public function apipostkhac($url,$data){
        if($json = @file_get_contents("http://14.160.70.230:11080/IcomESignature/swagger-ui.html#", true)){
            $curl = curl_init();
            $token=session('token');
            curl_setopt_array($curl, array(
                CURLOPT_PORT => "8080",
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS =>json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "AuthorizationIcomSMSTest: ".$token,
                    "Content-Type: application/json",
                ),
            ));
        return   ($response = curl_exec($curl));         
                 }
        else{
             return "";
            }
     }
     public function  apiget($url){
        if($json = @file_get_contents("http://14.160.70.230:11080/IcomESignature/swagger-ui.html#", true)){
            $token=session('token');;
            $opts = array('http' =>
                        array(
                            'method'  => 'GET',
                            'header'  => array("AuthorizationIcomSMSTest: ".$token), 
                           
                        )
                    );
                    $context  = stream_context_create($opts);
                    $result =  file_get_contents($url, true, $context);
        $res = json_decode($result);
        if($res == null){
            die("api return data null");
        }  
        if($res->status == -1 || $res->status == 0){ 
            echo "<a href='".asset('/')."'về trang đăng nhập</a>";
            die($res->msg);
        }
        return  $result;
        }
        else{
            return "";
                }
    }
    public function apipostadd($url,$data){
        $token=session('token');
        $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json',"AuthorizationIcomSMSTest: ".$token));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
        $res = json_decode($result);
        if($res == null){
            die("api return data null");
        }  
        if($res->status == -1 || $res->status == 0){
            echo "<a href='".asset('/')."'về trang đăng nhập</a>";
            die($res->msg);
        }
        return $result;
     }
     

}