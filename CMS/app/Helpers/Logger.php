<?php

namespace App\Helpers;

class Logger
{
    public function send($log)
    {
        return 'Sent!';
    }

    public static function log($log)
    {
        return app(Logger::class)->send($log);
    }
}