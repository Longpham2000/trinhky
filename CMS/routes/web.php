<?php

// use App\Helpers\ExternalApiHelper;
// use App\Helpers\Logger;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('admin')->group(function () {

//     Route::get('/dashboard', 'Admin\AdminController@index')->name('admin.dashboard');
//     Route::get('/approve', 'Admin\AdminController@approve')->name('admin.action.approve');
//     Route::get('/create', 'Admin\AdminController@create')->name('admin.action.create');
//     Route::get('/deny', 'Admin\AdminController@deny')->name('admin.action.deny');

//     Route::get('/user_list', 'Admin\AdminController@user_list')->name('admin.user.list');
//     Route::get('/room_list', 'Admin\AdminController@room_list')->name('admin.room.list');

//     Route::get('/process_list', 'Admin\AdminController@process_list')->name('admin.process_list');
//     Route::get('/approve_list', 'Admin\AdminController@approve_list')->name('admin.approve_list');

//     Route::get('/approve_his', 'Admin\AdminController@approve_his')->name('admin.approve_his');
//     Route::get('/approve_process', 'Admin\AdminController@approve_process')->name('admin.approve_process');
    

// });

// Route::get('/', function() {
//     // $apiHelper = new ExternalApiHelper('Hellp world!');
//     // return $apiHelper->foo();

//     // return app(ExternalApiHelper::class)->foo();

//     // return ExternalApiHelper::bar();

//     // $externalApi = ExternalApiHelper::setFoo('Hello, foo');
//     // $externalApiAgain = ExternalApiHelper::setFoo('Hello, foo again');
//     // return $externalApi->foo() . ' - ' . $externalApiAgain->foo();

//     return Logger::log('My app is broken');
// });

// Route::get('/', function() {
//     return "true";
// });
